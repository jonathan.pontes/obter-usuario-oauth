package com.oauth.lanche;

import com.oauth.lanche.security.Usuario;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsuarioController {

    @GetMapping("/")
    public Usuario obterUsuario(@AuthenticationPrincipal Usuario usuario) {
        return usuario;
    }


}
